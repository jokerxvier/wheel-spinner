/**
* Package use in spinner 
* https://josex2r.github.io/jQuery-SlotMachine/
*
* TODO - WHen the user type on the rundown text area it should appear in the #list-rundown 
* TODO - When the spin button click it should get the list of topics in #list-topic2 and append the values in the spinner
* TODO - List of topics should be active once the topic is already selected in the spinner
* TODO - Reset Button will reset the values
* TODO - Back Button should go back to the first page
*/
$(function () {

    const $dataList = $('.data-list');
    const runDown = [];
    let topics = [];
    let selectedRundown = [];
    let selectedTopic = [];

    /**
    * Append the added entry everytime the use type in rundown text area
    */
    $('#list-rundown').on('keyup', function () {
        let $li = '';
        let str = $(this).val().replace(/(?:\r\n|\r|\n)/g, ',').split(',');

        _.forEach(str, function (item) {
            if (item) {
                $li += `<li class="list-group-item">${item}</li>`;
            }
        });

        $dataList.html('');
        $dataList.append($li);
    });

    /**
    * Append the added entry everytime the use tipe in topic text area
    */
    $('#list-topic2').on('keyup', function () {
        topics = [];
        let str = $(this).val().replace(/(?:\r\n|\r|\n)/g, ',').split(',');

        let $li = '';


        if (str.length > 0) {
            _.forEach(str, function (item) {
                if (item) {
                    $li += `<div class="list">${item}</div>`;
                }
            });

            console.log($li);

            $('#machine').html('');
            $('#machine').append($li);
        }

    });

    /**
    * When the toggle-spin click hide the toggle button and show the spin button
    */
    $('#toggle-spin').on('click', function () {
        $(this).hide();
        $('#spin').show();

    });

    /**
    * When the #spin button click run the spinner
    */
    $('#spin').on('click', function () {

        const machine = $('#machine').slotMachine({
            delay: 500
        });

        machine.shuffle(5, onComplete);
    })

});

//Trigger when the spinner is completed
function onComplete(value) {
    console.log(value);
}